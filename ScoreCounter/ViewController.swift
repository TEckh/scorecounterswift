//
//  ViewController.swift
//  ScoreCounter
//
//  Created by Trevor Eckhardt on 7/2/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var stepper1: UIStepper!
    @IBOutlet var stepper2: UIStepper!
    @IBOutlet var count1: UILabel!
    @IBOutlet var count2: UILabel!
    @IBOutlet var reset: UIButton!
    @IBOutlet var Text1: UITextField!
    @IBOutlet var Text2: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func stepper1ValueChanged(sender: UIStepper) {
        count1.text = NSString(format: "%.0f", sender.value) as String;
//        count1.text = Int(sender.value).description
    }
    
    @IBAction func stepper2ValueChanged(sender: UIStepper) {
        count2.text = NSString(format: "%.0f", sender.value) as String;
//        count2.text = Int(sender.value).description
    }
    
    @IBAction func ResetButtonTouched(sender: UIButton) {
        count1.text = "0"
        count2.text = "0"
        stepper1.value = 0
        stepper2.value = 0
    }
    
    func textFieldShouldReturn(textField: UITextField) ->Bool{
        self.view.endEditing(true)
//        textField.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

